Exercicios 1:
	1: Criar um novo projeto usando a estrutura de pastas das boas práticas;
	2: Crie 2 arquivos .html, o index.html e o sobre.html. Onde index é a pagina inicial e o sobre será uma subpágina;
	3: Crie 3 arquivos .css chamados geral.css, index.css, sobre.css;
	4: Crie 2 arquivos .js chamados geral.js e index.js;
	5: No arquivo index.html:
	5.1: Coloque a estrutura de TAGs de boa prática para 'inicio' do projeto;
	5.2: Coloque as TAGs não visíveis ao usuário/navegador na respectiva TAG
	5.2.1: Defina o titulo do seu site (Aquele que aparece na nova aba do navegador)
	5.2.2: Defina que o navegador irá usar o padrão unicode (para não quebrar as acentuações)
	5.3: Insira uma imagem como banner
	5.4: Na parte principal siga a seguinte ordem e insira:
	5.4.1: Um titulo
	5.4.2: Um texto com 2 ou mais parágrafos
	5.4.3: Uma imagem
	5.4.4: Um texto
	5.4.5: Um subtitulo
	5.4.6: Um texto com 2 ou mais parágrafos
	5.4.7: Outro subtitulo
	5.4.8: Outro texto
	5.5: Insira uma imagem como rodapé
--------------------------------------------------------------------------------------------------------------------------
Exercicio 2:
	Continuando nos arquivos enviado compactados, faça as seguintes modificações:
	1: Corrigir as aspas que foram colocadas.
	2: No index.html, faça a referencia aos arquivos CSS criados. (<link>)
	3: Mude a cor das letras de todo o site para outra cor, ficando uma cor por padrão. (body {color: XXX})
	4: Mude a "font-family" de todas as letras do site. ficando uma fonte padrão. (body {font-family: XXX})
	5: Coloque algumas palavras ou frases em itálico e negrito. (span {font-style:	italic;})
	6: Na imagem do corpo, adicione uma legenda e coloque uma cor de fundo em toda a imagem e centralize a imagem. (figure {background-color: #F2EDED})
	7: No título principal, coloque uma cor de fundo.
	8: Nos subtitulos coloque um sublinhado.
	9: Insira 2 novos parágrafos maiores e centralize o texto deles.
	10: Coloque uma imagem de cabeçalho e troque a imagem de rodapé.

	Lembrando que é recomendado realizar boa parte das modificações solicitadas acima no aruivo .css